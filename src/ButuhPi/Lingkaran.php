<?php 

namespace Dummy\ButuhPi;

use Dummy\ButuhPi;

class Lingkaran implements ButuhPi
{
	const Pi = 22/7;
	private $jari_jari;

	function __construct($jari_jari)
	{
		return $this->jari_jari = $jari_jari;
	}

	public function hitungLuas()
	{
		return self::Pi * pow($this->jari_jari, 2) / 2;
	} 

	public function hitungKeliling()
	{
		return 2 * self::Pi * $this->jari_jari;
	}
}