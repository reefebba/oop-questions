<?php 

namespace Dummy;

use Dummy\KelilingSimetris;

abstract class BangunDatar
{
	use KelilingSimetris;

	abstract protected function hitungLuas();
}

