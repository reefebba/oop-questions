<?php 

namespace Dummy;

trait KelilingSimetris
{
	private $side_senilai;
	private $side1;
	private $side2;
	private $side3;

	function hitungKeliling($side_senilai,$side1,$side2=0,$side3=0)
	{
		$this->side1 = $side1;
		$this->side2 = $side2;
		$this->side3 = $side3;
		$this->side_senilai = $side_senilai;
		return ($this->side1 + $this->side2 + $this->side3) * $this->side_senilai;
	}
}
