<?php 

namespace Dummy\BangunDatar;

use Dummy\BangunDatar;

class Persegi extends BangunDatar
{
	private $sisi;
	
	function __construct($sisi)
	{
		return $this->sisi = $sisi;
	}

	public function getSisi()
	{
		return $this->sisi;
	}

	public function hitungLuas()
	{
		return pow($this->sisi, 2);
	} 
}