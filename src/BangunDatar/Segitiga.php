<?php 

namespace Dummy\BangunDatar;

use Dummy\BangunDatar;

class Segitiga extends BangunDatar
{
	private $alas;
	private $tinggi;
	
	function __construct($alas,$tinggi)
	{
		$this->alas = $alas;
		$this->tinggi = $tinggi;
	}

	public function hitungLuas()
	{
		return 0.5 * $this->alas * $this->tinggi;
	} 
}
