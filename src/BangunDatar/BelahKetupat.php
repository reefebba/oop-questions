<?php 

namespace Dummy\BangunDatar;

use Dummy\BangunDatar;

class BelahKetupat extends BangunDatar
{
	private $diagonal1;
	private $diagonal2;
	private $sisi;
	
	function __construct($diagonal1,$diagonal2)
	{
		$this->diagonal1 = $diagonal1;
		$this->diagonal2 = $diagonal2;
	}

	public function setSisi($sisi)
	{
		return $this->sisi = $sisi;
	}


	public function hitungLuas()
	{
		return $this->diagonal1 * $this->diagonal2 / 2;
	} 
}
