<?php 

namespace Dummy\BangunDatar;

use Dummy\BangunDatar;

class PersegiPanjang extends BangunDatar
{
	private $panjang;
	private $lebar;
	
	public function setPanjangLebar($panjang,$lebar)
	{
		$this->panjang = $panjang;
		$this->lebar = $lebar;
	}

	public function hitungLuas()
	{
		return $this->panjang * $this->lebar;
	} 
}