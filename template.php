<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Kalkulator Bangun Datar</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="judul">
    <h1>Kalkulator Bangun Datar</h1>
    </div>
    <hr>
    <form action="" method="POST">
        <h1>Persegi</h1>
        <input type="number" name="sisi" placeholder="Sisi">
        <div>
            <button class="btn-luas" type="submit" name="luas_persegi">Luas</button>
            <button class="btn-keliling" type="submit" name="keliling_persegi">Keliling</button>
            <div><?= $luas_persegi; ?></div>
            <div><?= $keliling_persegi; ?></div>
        </div>        
    </form>
    <form action="" method="POST">
        <h1>Persegi Panjang</h1>
        <input type="number" name="panjang_persegi_panjang" placeholder="Panjang">
        <input type="number" name="lebar_persegi_panjang" placeholder="Lebar">
        <div>
            <button class="btn-luas" type="submit" name="luas_persegi_panjang">Luas</button>
            <button class="btn-keliling" type="submit" name="keliling_persegi_panjang">Keliling</button>
            <div><?= $luas_persegi_panjang; ?></div>
            <div><?= $keliling_persegi_panjang; ?></div>
        </div>
    </form>
    <form action="" method="POST">
        <h1>Segitiga</h1>
        <input type="number" name="alas" placeholder="Alas">
        <input type="number" name="tinggi" placeholder="Tinggi">
        <input type="number" name="sisi1" placeholder="Sisi Miring">
        <input type="number" name="sisi2" placeholder="Sisi Miring">
        <div>
            <button class="btn-luas" type="submit" name="luas_segitiga">Luas</button>
            <button class="btn-keliling" type="submit" name="keliling_segitiga">Keliling</button>
            <div><?= $luas_segitiga; ?></div>
            <div><?= $keliling_segitiga; ?></div>
        </div>        
    </form>
    <form action="" method="POST">
        <h1>Belah Ketupat</h1>
        <input type="number" name="diagonal1" placeholder="Diagonal1">
        <input type="number" name="diagonal2" placeholder="Diagonal2">
        <input type="number" name="sisi" placeholder="Sisi">
        <div>
            <button class="btn-luas" type="submit" name="luas_belah_ketupat">Luas</button>
            <button class="btn-keliling" type="submit" name="keliling_belah_ketupat">Keliling</button>
            <div><?= $luas_belah_ketupat; ?></div>
            <div><?= $keliling_belah_ketupat; ?></div>
        </div>        
    </form>
    <form action="" method="POST">
        <h1>Lingkaran</h1>
        <input type="number" name="jari_jari" placeholder="Jari-jari">
        <div>
            <button class="btn-luas" type="submit" name="luas_lingkaran">Luas</button>
            <button class="btn-keliling" type="submit" name="keliling_lingkaran">Keliling</button>
            <div><?= $luas_lingkaran; ?></div>
            <div><?= $keliling_lingkaran; ?></div>
        </div>        
    </form>
</body>
</html>