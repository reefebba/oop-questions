<?php 
// require 'functions.php'; 
require 'vendor/autoload.php';

use Dummy\BangunDatar\Persegi;
use Dummy\BangunDatar\PersegiPanjang;
use Dummy\BangunDatar\Segitiga;
use Dummy\BangunDatar\BelahKetupat;
use Dummy\ButuhPi\Lingkaran;

ob_start();
if (isset($_POST['luas_persegi'])) {
    $sisi = htmlspecialchars($_POST['sisi']);
    $persegi = new Persegi($sisi);
    echo "Luas Persegi = " . $persegi->hitungLuas();
}
$luas_persegi = ob_get_clean();

ob_start();
if (isset($_POST['keliling_persegi'])) {
    $sisi = htmlspecialchars($_POST['sisi']);
    $persegi = new Persegi($sisi);
    echo "Keliling Persegi = " . $persegi->hitungKeliling(4,$sisi);
}
$keliling_persegi = ob_get_clean();

ob_start();
if (isset($_POST['luas_persegi_panjang'])) {
    $panjang = htmlspecialchars($_POST['panjang_persegi_panjang']);
    $lebar = htmlspecialchars($_POST['lebar_persegi_panjang']);
    $persegipanjang = new PersegiPanjang();
	$persegipanjang->setPanjangLebar($panjang,$lebar);
	echo "Luas Persegi Panjang = " . $persegipanjang->hitungLuas();
}
$luas_persegi_panjang = ob_get_clean();

ob_start();
if (isset($_POST['keliling_persegi_panjang'])) {
    $panjang = htmlspecialchars($_POST['panjang_persegi_panjang']);
    $lebar = htmlspecialchars($_POST['lebar_persegi_panjang']);
    $persegipanjang = new PersegiPanjang();
	$persegipanjang->setPanjangLebar($panjang,$lebar);
	echo "Keliling Persegi Panjang = " . $persegipanjang->hitungKeliling(2,$panjang,$lebar);
}
$keliling_persegi_panjang = ob_get_clean();

ob_start();
if (isset($_POST['luas_segitiga'])) {
    $alas = htmlspecialchars($_POST['alas']);
    $tinggi = htmlspecialchars($_POST['tinggi']);
    $segitiga = new Segitiga($alas,$tinggi);
	echo "Luas Segitiga = " . $segitiga->hitungLuas();
}
$luas_segitiga = ob_get_clean();

ob_start();
if (isset($_POST['keliling_segitiga'])) {
    $alas = htmlspecialchars($_POST['alas']);
    $tinggi = htmlspecialchars($_POST['tinggi']);
    $sisi1 = htmlspecialchars($_POST['sisi1']);
    $sisi2 = htmlspecialchars($_POST['sisi2']);
    $segitiga = new Segitiga($alas,$tinggi);
	echo "Keliling Segitiga = " . $segitiga->hitungKeliling(1,$alas,$sisi1,$sisi2);
}
$keliling_segitiga = ob_get_clean();

ob_start();
if (isset($_POST['luas_belah_ketupat'])) {
    $diagonal1 = htmlspecialchars($_POST['diagonal1']);
    $diagonal2 = htmlspecialchars($_POST['diagonal2']);
    $belah_ketupat = new BelahKetupat($diagonal1,$diagonal2);
	echo "Luas Belah Ketupat = " . $belah_ketupat->hitungLuas();
}
$luas_belah_ketupat = ob_get_clean();

ob_start();
if (isset($_POST['keliling_belah_ketupat'])) {
    $diagonal1 = htmlspecialchars($_POST['diagonal1']);
    $diagonal2 = htmlspecialchars($_POST['diagonal2']);
    $sisi = htmlspecialchars($_POST['sisi']);
    $belah_ketupat = new BelahKetupat($diagonal1,$diagonal2);
	echo "Keliling Belah Ketupat = " . $belah_ketupat->hitungKeliling(4,$sisi);
}
$keliling_belah_ketupat = ob_get_clean();

ob_start();
if (isset($_POST['luas_lingkaran'])) {
    $jari_jari = htmlspecialchars($_POST['jari_jari']);
    $lingkaran = new Lingkaran($jari_jari);
    echo "Luas lingkaran = " . $lingkaran->hitungLuas();
}
$luas_lingkaran = ob_get_clean();

ob_start();
if (isset($_POST['keliling_lingkaran'])) {
    $jari_jari = htmlspecialchars($_POST['jari_jari']);
    $lingkaran = new Lingkaran($jari_jari);
    echo "Keliling lingkaran = " . $lingkaran->hitungKeliling();
}
$keliling_lingkaran = ob_get_clean();
include 'template.php';

// $persegi = new Persegi(5);
// $sisi = $persegi->getSisi();
// echo "Luas Persegi =" . $persegi->hitungLuas() . "<br>";
// echo "Keliling Persegi =" . $persegi->hitungKeliling(4,$sisi);
// echo "<hr>";

// $persegipanjang = new PersegiPanjang();
// $persegipanjang->setPanjangLebar(7,5);
// echo "Luas Persegi =" . $persegipanjang->hitungLuas() . "<br>";
// echo "Keliling Persegi =" . $persegipanjang->hitungKeliling(2,7,5);
// echo "<hr>";

// $segitiga = new Segitiga(5,12);
// echo "Luas segitiga =" . $segitiga->hitungLuas() . "<br>";
// echo "Keliling segitiga =" . $segitiga->hitungKeliling(1,5,12,13);
// echo "<hr>";

// $belahketupat = new BelahKetupat(6,7);
// echo "Luas belahketupat =" . $belahketupat->hitungLuas() . "<br>";
// $sisibk = $belahketupat->setSisi(6);
// echo "Keliling belahketupat =" . $belahketupat->hitungKeliling(4,$sisibk);
// echo "<hr>";

// $lingkaran = new Lingkaran(7);
// echo "Luas lingkaran =" . $lingkaran->hitungLuas() . "<br>";
// echo "Keliling lingkaran =" . $lingkaran->hitungKeliling();
// echo "<hr>";
