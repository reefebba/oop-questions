# What is this all about? 
This is a **proposed solution** for a problem given as my evaluation test after probably 2 months of learning *how to code*, what a mess!! :laughing:

### Roles

1. Coding style harus berdasarkan [PSR](https://www.php-fig.org/psr/)
2. Harus memenuhi [SOLID](https://en.wikipedia.org/wiki/SOLID) principle ( minimal S )
3. Harus terdapat beberapa OOP konsep, seperti:
    - Setter & Getter
    - Abstract Class / Method
    - Inheritance
    - Interface
    - Visibility Property / Method
4. Gunakan autoload composer

### Question

Buat Class untuk menghitung Luas dan Keliling bangun datar:
  - Persegi
  - Persegi Panjang
  - Lingkaran
  - Belah Ketupat
  - Segitiga

### Screenshot

 ![](/FireShot_Capture_001_-_Kalkulator_Bangun_Datar_-_http___localhost_oop-questions_.png)
